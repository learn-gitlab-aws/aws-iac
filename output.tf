output "EventBusARN" {
  value       = aws_cloudwatch_event_bus.data_pipeline_bus.arn
  description = "Event bridge output"
}

output "EventRuleARN" {
  value       = aws_cloudwatch_event_target.cloudwatch.arn
  description = "Event rule output"
}

output "CloudWatchLog" {
  value       = aws_cloudwatch_log_group.cloud_watch_log_group.arn
  description = "ARN from Cloud Watch Log Group"
}
