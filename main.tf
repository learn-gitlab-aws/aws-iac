resource "aws_cloudwatch_event_bus" "data_pipeline_bus" {
  name = "data_pipline_bus"
 # policy = event-bus-policy.json
}

resource "aws_cloudwatch_event_rule" "data_pipeline_rule" {
  name        = "data_pipeline_rule"
  description = "Capture each AWS Console Sign In"

  event_pattern = <<EOF
	{
 		 "source": ["aws.s3"],
		 "detail-type": ["AWS API Call via CloudTrail"],
		 "detail": {
			    "eventSource": ["s3.amazonaws.com"],
			    "eventName": ["PutObject", "DeleteObjects", "DeleteObject"]
			}
	}
	EOF
}

resource "aws_cloudwatch_event_target" "cloudwatch" {
  rule      = aws_cloudwatch_event_rule.data_pipeline_rule.name
  target_id = "SendToCloudWatch"
  arn       = aws_cloudwatch_log_group.cloud_watch_log_group.arn
}

resource "aws_cloudwatch_log_group" "cloud_watch_log_group" {
  name = "/aws/events/event_log_group_datapipeline"
  tags = var.custom_tags 
  #tags = {
  #  Environment = "production"
  #  Application = "serviceA"
  #}
}
